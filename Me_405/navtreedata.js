/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "My Project", "index.html", [
    [ "Introduction", "index.html#intro_sec", null ],
    [ "Lab01", "index.html#Lab01_sec", null ],
    [ "Lab02", "index.html#Lab02_sec", null ],
    [ "Lab03", "index.html#Lab03_sec", null ],
    [ "Lab04", "index.html#Lab04_sec", null ],
    [ "Lab05 and Lab06", "index.html#Lab056_sec", null ],
    [ "Lab07", "index.html#Lab07_sec", null ],
    [ "Lab08", "index.html#Lab08_sec", null ],
    [ "Lab09", "index.html#Lab09_sec", null ],
    [ "Lab05 Kinematics", "Lab05.html", [
      [ "Introduction and Assumptions", "Lab05.html#Assumption_sec", null ],
      [ "Equations of Motion", "Lab05.html#Eom_sec", null ],
      [ "Kinematics", "Lab05.html#Kinematics_sec", null ],
      [ "Analytical Model", "Lab05.html#Matrix_sec", null ]
    ] ],
    [ "Lab06 System Modeling", "Lab06_sec.html", [
      [ "Linearization of model", "Lab06_sec.html#Introduction_sec", null ],
      [ "Simulation Model", "Lab06_sec.html#Sim_sec", null ],
      [ "Open Loop System Responses", "Lab06_sec.html#OpenScenarios_sec", null ],
      [ "Closed Loop System Response", "Lab06_sec.html#ClosedScenarios_sec", null ]
    ] ],
    [ "Lab07 Resistive Touch Panel Driver", "Lab07_sec.html", [
      [ "Background and objectives", "Lab07_sec.html#Background_sec", null ],
      [ "Testing", "Lab07_sec.html#Testing_sec", null ]
    ] ],
    [ "Balance Board Project", "Lab09_sec.html", [
      [ "Background on Balance Board", "Lab09_sec.html#Intro_sec", null ],
      [ "Drivers", "Lab09_sec.html#Drivers_sec", null ],
      [ "bno055 sensor", "Lab09_sec.html#bno055_sec", null ],
      [ "Controller", "Lab09_sec.html#Controller_sec", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"CLDriver_8py.html",
"classMotor__Driver_1_1MotorDriver.html#a4fbf50c71883feb3229f12f249f2d08a"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';