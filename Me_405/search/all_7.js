var searchData=
[
  ['gearratio_36',['gearRatio',['../classEncoderDriver_1_1EncoderDriver.html#a5b2d3bdd66a6d50cf9adab298a777147',1,'EncoderDriver.EncoderDriver.gearRatio()'],['../classMotorEncoder_1_1EncoderDriver.html#a8da915995190d000fe225e966c85e151',1,'MotorEncoder.EncoderDriver.gearRatio()']]],
  ['getangle_37',['getAngle',['../classEncoderDriver_1_1EncoderDriver.html#af7d61bb0a757b44953d59a6f2da7fd24',1,'EncoderDriver::EncoderDriver']]],
  ['getchange_38',['getChange',['../Lab01_8py.html#a5eecadc0f6b533b3517104dad5a183a8',1,'Lab01']]],
  ['getdelta_39',['getDelta',['../classEncoderDriver_1_1EncoderDriver.html#ae3f6da88f85d2c70d966de302e7ba728',1,'EncoderDriver.EncoderDriver.getDelta()'],['../classMotorEncoder_1_1EncoderDriver.html#a24ec663333ac2a4314c9e52ca3b6c6a5',1,'MotorEncoder.EncoderDriver.getDelta()']]],
  ['getpayment_40',['getPayment',['../Lab01_8py.html#ab662bb4f81a4b79305661ed3d531304f',1,'Lab01']]],
  ['getposition_41',['getPosition',['../classEncoderDriver_1_1EncoderDriver.html#ab7a8a5da360e77ad0c7cedf6b0972866',1,'EncoderDriver.EncoderDriver.getPosition()'],['../classMotorEncoder_1_1EncoderDriver.html#aba2de0541b9ee47604a2bc132cd977ee',1,'MotorEncoder.EncoderDriver.getPosition()']]],
  ['getspeed_42',['getSpeed',['../classEncoderDriver_1_1EncoderDriver.html#a6866a06baba74d26c3034d0ad43ec2b5',1,'EncoderDriver::EncoderDriver']]]
];
