var searchData=
[
  ['theta_5fx_272',['theta_x',['../classCLTask_1_1CLTask.html#a416a74e857c2d9ae4831d2996130c78d',1,'CLTask::CLTask']]],
  ['theta_5fx_5fdot_273',['theta_x_dot',['../classCLTask_1_1CLTask.html#a2ac1e37bdd24f929e4bb92e0d1d7c803',1,'CLTask::CLTask']]],
  ['theta_5fy_274',['theta_y',['../classCLTask_1_1CLTask.html#a31feabf8354726ee6bf19b79ec8f7b51',1,'CLTask::CLTask']]],
  ['theta_5fy_5fdot_275',['theta_y_dot',['../classCLTask_1_1CLTask.html#a20b250dc60fe3a1113a23d41fdeef5f4',1,'CLTask::CLTask']]],
  ['tim_276',['tim',['../classEncoderDriver_1_1EncoderDriver.html#a757722422a8e60f631d9b1a97744992d',1,'EncoderDriver.EncoderDriver.tim()'],['../classMotorEncoder_1_1EncoderDriver.html#ad5124af9ea4e557cc665ce186bfaa36d',1,'MotorEncoder.EncoderDriver.tim()']]],
  ['timch1_277',['timch1',['../classMotorDriver_1_1MotorDriver.html#a63fde1bfdb9880858e0a2e39b5f15794',1,'MotorDriver.MotorDriver.timch1()'],['../classYoung__MotorDriver_1_1MotorDriver.html#a529e166bcf1b5080157c4bdb7a82d228',1,'Young_MotorDriver.MotorDriver.timch1()']]],
  ['timch2_278',['timch2',['../classMotor__Driver_1_1MotorDriver.html#a26e1005074fe2b432da42aaf5e72ed77',1,'Motor_Driver.MotorDriver.timch2()'],['../classMotorDriver_1_1MotorDriver.html#ab3b2e60b876b4ab28b1b8cbe34b68132',1,'MotorDriver.MotorDriver.timch2()'],['../classYoung__MotorDriver_1_1MotorDriver.html#aad69b9e31e1a6f7f3088ce6c666231dd',1,'Young_MotorDriver.MotorDriver.timch2()']]],
  ['timearray_279',['timeArray',['../classCLTask_1_1CLTask.html#a1387a2a65eb7162e4bbbf7fe201720e8',1,'CLTask::CLTask']]],
  ['timer_280',['timer',['../classMotor__Driver_1_1MotorDriver.html#a95cec2c1094bb4bc28e9f4503820d1c4',1,'Motor_Driver.MotorDriver.timer()'],['../classMotorDriver_1_1MotorDriver.html#ab01a28fc3b6e0720c1d9922ac16a4010',1,'MotorDriver.MotorDriver.timer()'],['../classYoung__MotorDriver_1_1MotorDriver.html#a1c0151c41d32d9389e8a387cbdd5937f',1,'Young_MotorDriver.MotorDriver.timer()']]],
  ['touchobject_281',['TouchObject',['../classCLTask_1_1CLTask.html#a69d30c338aa512290eafcf01a93a3728',1,'CLTask::CLTask']]]
];
