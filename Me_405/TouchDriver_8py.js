var TouchDriver_8py =
[
    [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ],
    [ "center", "TouchDriver_8py.html#a1439b02b478bfc3701aae4befe80ce03", null ],
    [ "endTime", "TouchDriver_8py.html#a491af27bbaf9bc51c9d34f63c7fdb03d", null ],
    [ "length", "TouchDriver_8py.html#ac19d655a4969ad67103c6f38bb918e8f", null ],
    [ "pinxm", "TouchDriver_8py.html#a28879fb683b3d74ee5d759bdb514363e", null ],
    [ "pinxp", "TouchDriver_8py.html#a9c69f268a60fa070fccef2f8ce5012f9", null ],
    [ "pinym", "TouchDriver_8py.html#a1f4e857de33a21e93149bed43126c8c1", null ],
    [ "pinyp", "TouchDriver_8py.html#a9655b883800394c9e97939c10d150fd5", null ],
    [ "readTest", "TouchDriver_8py.html#a7b1731d46875c5fa634e06fc195b52b1", null ],
    [ "startTime", "TouchDriver_8py.html#a686f0eb71a9ca8a3e82afaf62322c544", null ],
    [ "testTime", "TouchDriver_8py.html#acb1113b056f35d7c7754fc2e275f516b", null ],
    [ "timeAvg", "TouchDriver_8py.html#ac5534a038f44b58192728723a1e6670c", null ],
    [ "timeSum", "TouchDriver_8py.html#a921cecd55cbf6814603953c217f7e9e8", null ],
    [ "touchObject", "TouchDriver_8py.html#ae56883da6e56963f7ab9564abbc2e79b", null ],
    [ "width", "TouchDriver_8py.html#a3fb4865c9b4d92ae304938fcec49987c", null ],
    [ "xTest", "TouchDriver_8py.html#a1aaf7d253fae8c810632e553414df037", null ],
    [ "yTest", "TouchDriver_8py.html#a308eea2973f06d3d03cc52b344ce50c3", null ],
    [ "zTest", "TouchDriver_8py.html#a3f86b16467e0e7ae34568c671670ab77", null ]
];