var Motor__Driver_8py =
[
    [ "MotorDriver", "classMotor__Driver_1_1MotorDriver.html", "classMotor__Driver_1_1MotorDriver" ],
    [ "channel1", "Motor__Driver_8py.html#abf65b71c4188c228363ccbdd599e672a", null ],
    [ "channel2", "Motor__Driver_8py.html#a649d00769623ebb60a0a0352d155222d", null ],
    [ "cmd", "Motor__Driver_8py.html#a4aba46b78a22b49d9206e91a65472f95", null ],
    [ "fault_status", "Motor__Driver_8py.html#a74a752a0567da07b75df0ecb8ea8ee1e", null ],
    [ "IN1_pin", "Motor__Driver_8py.html#a78d9b789f9ebe9d51f999eecfb057f8c", null ],
    [ "IN2_pin", "Motor__Driver_8py.html#a63f61c968ad97afbc3508ed2880b3f89", null ],
    [ "motor1", "Motor__Driver_8py.html#a5f62ad31073c66f39cffd2fca02cedab", null ],
    [ "motorNum", "Motor__Driver_8py.html#af246e753e9321a175374d22f19203c90", null ],
    [ "nFAULT_pin", "Motor__Driver_8py.html#a08714578f4c5a349a7bfdf269a820f79", null ],
    [ "nSLEEP_pin", "Motor__Driver_8py.html#a085b1fef103adb67cfea002ceebd473b", null ],
    [ "timer", "Motor__Driver_8py.html#a9735cac8d753a899fa2132afa2ee4978", null ]
];