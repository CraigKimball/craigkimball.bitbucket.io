var annotated_dup =
[
    [ "CLDriver", null, [
      [ "CLDriver", "classCLDriver_1_1CLDriver.html", "classCLDriver_1_1CLDriver" ]
    ] ],
    [ "CLTask", null, [
      [ "CLTask", "classCLTask_1_1CLTask.html", "classCLTask_1_1CLTask" ]
    ] ],
    [ "EncoderDriver", null, [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab07", null, [
      [ "TouchyPanel", "classLab07_1_1TouchyPanel.html", "classLab07_1_1TouchyPanel" ]
    ] ],
    [ "mcp9808_craig", null, [
      [ "TempyGet", "classmcp9808__craig_1_1TempyGet.html", "classmcp9808__craig_1_1TempyGet" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "MotorDriver", "classMotor__Driver_1_1MotorDriver.html", "classMotor__Driver_1_1MotorDriver" ]
    ] ],
    [ "MotorDriver", null, [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorEncoder", null, [
      [ "EncoderDriver", "classMotorEncoder_1_1EncoderDriver.html", "classMotorEncoder_1_1EncoderDriver" ]
    ] ],
    [ "TouchDriver", null, [
      [ "TouchDriver", "classTouchDriver_1_1TouchDriver.html", "classTouchDriver_1_1TouchDriver" ]
    ] ],
    [ "Young_MotorDriver", null, [
      [ "MotorDriver", "classYoung__MotorDriver_1_1MotorDriver.html", "classYoung__MotorDriver_1_1MotorDriver" ]
    ] ]
];