var classMotor__Driver_1_1MotorDriver =
[
    [ "__init__", "classMotor__Driver_1_1MotorDriver.html#a00575721ee905ccb9cfa00d2f1efd3c5", null ],
    [ "disable", "classMotor__Driver_1_1MotorDriver.html#abd4507fc3fdac3fc9a10b2d6fa7c6608", null ],
    [ "enable", "classMotor__Driver_1_1MotorDriver.html#af6576759bd1844dc919e0f3e7ca9c1f9", null ],
    [ "faultInterrupt", "classMotor__Driver_1_1MotorDriver.html#a4fbf50c71883feb3229f12f249f2d08a", null ],
    [ "set_duty", "classMotor__Driver_1_1MotorDriver.html#a6900ec5e218f96f0bb2386dccf8a6c3e", null ],
    [ "fault_status", "classMotor__Driver_1_1MotorDriver.html#ae88a9a1bc524820f650d2e5985aa76c1", null ],
    [ "IN1_pin", "classMotor__Driver_1_1MotorDriver.html#af40193d707228920a8dfe6a9c7ee3ec3", null ],
    [ "IN2_pin", "classMotor__Driver_1_1MotorDriver.html#a0b0aacd4e0cac3ac21f48e893c245639", null ],
    [ "motorNum", "classMotor__Driver_1_1MotorDriver.html#a44da12bc12ccb6fd6ef668deca4a2a6b", null ],
    [ "nFault_Pin", "classMotor__Driver_1_1MotorDriver.html#abdbd10c7a3d647cff0998dddde81b4ed", null ],
    [ "nSleep_pin", "classMotor__Driver_1_1MotorDriver.html#a2540c6b7c3b8ac7cc12df9e5b90296b5", null ],
    [ "timch1", "classMotor__Driver_1_1MotorDriver.html#ad0c7e43e6fb0df2b2be709edb69c6077", null ],
    [ "timch2", "classMotor__Driver_1_1MotorDriver.html#a26e1005074fe2b432da42aaf5e72ed77", null ],
    [ "timer", "classMotor__Driver_1_1MotorDriver.html#a95cec2c1094bb4bc28e9f4503820d1c4", null ]
];