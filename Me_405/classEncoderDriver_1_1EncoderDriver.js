var classEncoderDriver_1_1EncoderDriver =
[
    [ "__init__", "classEncoderDriver_1_1EncoderDriver.html#ac2e7a64634a6892df8bf857407d27c30", null ],
    [ "getAngle", "classEncoderDriver_1_1EncoderDriver.html#af7d61bb0a757b44953d59a6f2da7fd24", null ],
    [ "getDelta", "classEncoderDriver_1_1EncoderDriver.html#ae3f6da88f85d2c70d966de302e7ba728", null ],
    [ "getPosition", "classEncoderDriver_1_1EncoderDriver.html#ab7a8a5da360e77ad0c7cedf6b0972866", null ],
    [ "getSpeed", "classEncoderDriver_1_1EncoderDriver.html#a6866a06baba74d26c3034d0ad43ec2b5", null ],
    [ "setPosition", "classEncoderDriver_1_1EncoderDriver.html#a1c8936c8515651946404846825345b2b", null ],
    [ "tick2rad", "classEncoderDriver_1_1EncoderDriver.html#a72590cbc55ea0808169e1b7bb1b122d4", null ],
    [ "update", "classEncoderDriver_1_1EncoderDriver.html#a801d099176eaeb5ae628fef95f978680", null ],
    [ "CPR", "classEncoderDriver_1_1EncoderDriver.html#a0b47adbc13b3daa34acd23b738a9ff90", null ],
    [ "curr_count", "classEncoderDriver_1_1EncoderDriver.html#a5cd371dd43c6e789b48e1aab8cf28c16", null ],
    [ "delta", "classEncoderDriver_1_1EncoderDriver.html#a60a87a2342a68b354f7dd83f7fe08b2b", null ],
    [ "gearRatio", "classEncoderDriver_1_1EncoderDriver.html#a5b2d3bdd66a6d50cf9adab298a777147", null ],
    [ "overflow", "classEncoderDriver_1_1EncoderDriver.html#a2c0a294a3efde8483815aa80f071a7af", null ],
    [ "pinA", "classEncoderDriver_1_1EncoderDriver.html#ac4f9caf1e08c2cdea4485eaf6b116708", null ],
    [ "pinB", "classEncoderDriver_1_1EncoderDriver.html#a64d455a380049bbb5693968f51d28087", null ],
    [ "position", "classEncoderDriver_1_1EncoderDriver.html#a3ff85fbd31dcb3aa8d7aa588fba8c017", null ],
    [ "PPC", "classEncoderDriver_1_1EncoderDriver.html#a32f553339e8028f96dfc9d196a633259", null ],
    [ "prev_count", "classEncoderDriver_1_1EncoderDriver.html#aa58a3d97c2a5603edb9b09b2fe6a01a7", null ],
    [ "tim", "classEncoderDriver_1_1EncoderDriver.html#a757722422a8e60f631d9b1a97744992d", null ]
];