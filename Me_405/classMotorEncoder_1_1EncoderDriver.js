var classMotorEncoder_1_1EncoderDriver =
[
    [ "__init__", "classMotorEncoder_1_1EncoderDriver.html#ab1d24c075c3ea836dc9a1d7e210335ab", null ],
    [ "getDelta", "classMotorEncoder_1_1EncoderDriver.html#a24ec663333ac2a4314c9e52ca3b6c6a5", null ],
    [ "getPosition", "classMotorEncoder_1_1EncoderDriver.html#aba2de0541b9ee47604a2bc132cd977ee", null ],
    [ "setPosition", "classMotorEncoder_1_1EncoderDriver.html#a7f48163be2d38e0375d73fdf784946c9", null ],
    [ "tick2deg", "classMotorEncoder_1_1EncoderDriver.html#af6c33c6bf6e87a89a1cc49fec82c1ed5", null ],
    [ "tick2rpm", "classMotorEncoder_1_1EncoderDriver.html#af1f4b7a6671917cd44327697407d1951", null ],
    [ "update", "classMotorEncoder_1_1EncoderDriver.html#a6ca1ede4ff9faf831f7a6c4ab5c39482", null ],
    [ "CPR", "classMotorEncoder_1_1EncoderDriver.html#ac437a65d9ea929d1c6c63ab91aee3f45", null ],
    [ "curr_count", "classMotorEncoder_1_1EncoderDriver.html#afd0e3a4b69dcba856e3a6aaf4e03631d", null ],
    [ "delta", "classMotorEncoder_1_1EncoderDriver.html#a31ed8cd86bbe1fd76b216b94322d521e", null ],
    [ "gearRatio", "classMotorEncoder_1_1EncoderDriver.html#a8da915995190d000fe225e966c85e151", null ],
    [ "overflow", "classMotorEncoder_1_1EncoderDriver.html#ae3706a10b150bfceda80645e72d826b6", null ],
    [ "pinA", "classMotorEncoder_1_1EncoderDriver.html#a1f4b8c795ea4a675656dcb298da7327e", null ],
    [ "pinB", "classMotorEncoder_1_1EncoderDriver.html#ad369b503784c95293c87f8a01ac4565d", null ],
    [ "position", "classMotorEncoder_1_1EncoderDriver.html#a488d3a3f755c2d05df2139861dd5518c", null ],
    [ "PPC", "classMotorEncoder_1_1EncoderDriver.html#aedaa335f95c875571c1f57ae0fee2f6c", null ],
    [ "prev_count", "classMotorEncoder_1_1EncoderDriver.html#aefa6f59906ac48ce78b33eb3e1fd5a2d", null ],
    [ "tim", "classMotorEncoder_1_1EncoderDriver.html#ad5124af9ea4e557cc665ce186bfaa36d", null ]
];