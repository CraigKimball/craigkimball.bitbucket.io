var classYoung__MotorDriver_1_1MotorDriver =
[
    [ "__init__", "classYoung__MotorDriver_1_1MotorDriver.html#a9a8c249f173dd429ad93f88d29888666", null ],
    [ "brake", "classYoung__MotorDriver_1_1MotorDriver.html#af3c7a5fa8f10b44932507dbe488ae218", null ],
    [ "disable", "classYoung__MotorDriver_1_1MotorDriver.html#a83a98576ec057f5b1b6fb52016eaa3a7", null ],
    [ "enable", "classYoung__MotorDriver_1_1MotorDriver.html#afe583ddfbe3b092c27ace831bd8d5279", null ],
    [ "faultInterrupt", "classYoung__MotorDriver_1_1MotorDriver.html#a1dcda2eea6d9bad6b0e72ad1ffcd5349", null ],
    [ "setDuty", "classYoung__MotorDriver_1_1MotorDriver.html#ad89d5812c101d26da07b552ce42e0ff2", null ],
    [ "extint", "classYoung__MotorDriver_1_1MotorDriver.html#a5445961630070c667c705b9a173ae533", null ],
    [ "faultFlag", "classYoung__MotorDriver_1_1MotorDriver.html#ab2c9ed350b8fc154780d3f24bdce6ccb", null ],
    [ "motorNum", "classYoung__MotorDriver_1_1MotorDriver.html#a603ed887202e7ae86fdae004cea2f9c3", null ],
    [ "pinFault", "classYoung__MotorDriver_1_1MotorDriver.html#a6366bfdeb3dba482a9b49296a20f3e98", null ],
    [ "pinIN1", "classYoung__MotorDriver_1_1MotorDriver.html#a91cce21dc474bd27854034b3450cb8fd", null ],
    [ "pinIN2", "classYoung__MotorDriver_1_1MotorDriver.html#a444a018af711b1b3d027cb2c91389754", null ],
    [ "pinSleep", "classYoung__MotorDriver_1_1MotorDriver.html#aa89b4f1afa87e4f7f2eb186b80ea97a2", null ],
    [ "timch1", "classYoung__MotorDriver_1_1MotorDriver.html#a529e166bcf1b5080157c4bdb7a82d228", null ],
    [ "timch2", "classYoung__MotorDriver_1_1MotorDriver.html#aad69b9e31e1a6f7f3088ce6c666231dd", null ],
    [ "timer", "classYoung__MotorDriver_1_1MotorDriver.html#a1c0151c41d32d9389e8a387cbdd5937f", null ]
];