var files_dup =
[
    [ "CLDriver.py", "CLDriver_8py.html", [
      [ "CLDriver", "classCLDriver_1_1CLDriver.html", "classCLDriver_1_1CLDriver" ]
    ] ],
    [ "CLTask.py", "CLTask_8py.html", [
      [ "CLTask", "classCLTask_1_1CLTask.html", "classCLTask_1_1CLTask" ]
    ] ],
    [ "EncoderDriver.py", "EncoderDriver_8py.html", [
      [ "EncoderDriver", "classEncoderDriver_1_1EncoderDriver.html", "classEncoderDriver_1_1EncoderDriver" ]
    ] ],
    [ "Lab01.py", "Lab01_8py.html", "Lab01_8py" ],
    [ "Lab02.py", "Lab02_8py.html", "Lab02_8py" ],
    [ "Lab03_Back.py", "Lab03__Back_8py.html", "Lab03__Back_8py" ],
    [ "Lab03_Front.py", "Lab03__Front_8py.html", "Lab03__Front_8py" ],
    [ "Lab04_main.py", "Lab04__main_8py.html", "Lab04__main_8py" ],
    [ "Lab05_hand_page.py", "Lab05__hand__page_8py.html", null ],
    [ "Lab06_controls.py", "Lab06__controls_8py.html", null ],
    [ "Lab07.py", "Lab07_8py.html", [
      [ "TouchyPanel", "classLab07_1_1TouchyPanel.html", "classLab07_1_1TouchyPanel" ]
    ] ],
    [ "Lab09_doc.py", "Lab09__doc_8py.html", null ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "MotorDriver.py", "MotorDriver_8py.html", [
      [ "MotorDriver", "classMotorDriver_1_1MotorDriver.html", "classMotorDriver_1_1MotorDriver" ]
    ] ],
    [ "MotorEncoder.py", "MotorEncoder_8py.html", [
      [ "EncoderDriver", "classMotorEncoder_1_1EncoderDriver.html", "classMotorEncoder_1_1EncoderDriver" ]
    ] ],
    [ "TouchDriver.py", "TouchDriver_8py.html", "TouchDriver_8py" ]
];