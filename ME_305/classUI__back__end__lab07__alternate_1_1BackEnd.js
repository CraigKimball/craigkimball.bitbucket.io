var classUI__back__end__lab07__alternate_1_1BackEnd =
[
    [ "__init__", "classUI__back__end__lab07__alternate_1_1BackEnd.html#aba1065f29a8b4cb411c164ca684ce1c3", null ],
    [ "run", "classUI__back__end__lab07__alternate_1_1BackEnd.html#afd1b8093bdf9c19016eb2c9a5694b36e", null ],
    [ "transitionTo", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a6de179b70c14fbdf4af0183716ae1a0d", null ],
    [ "curr_time", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a14f73340e1d9f0c57dd7d1139f7f49da", null ],
    [ "data_counter", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a7879aa3e7a83e4fd18cd519d85e14ed4", null ],
    [ "interval", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a8eca00ed35dd9ca177d6001de8568404", null ],
    [ "K_p", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a731b69e3091f7bbb646971569aaf2ea8", null ],
    [ "next_time", "classUI__back__end__lab07__alternate_1_1BackEnd.html#aab24452311d9bee23fdc4e69593cd246", null ],
    [ "position_send", "classUI__back__end__lab07__alternate_1_1BackEnd.html#ab316f3d17aa1a31134685e10acc66fc4", null ],
    [ "runs", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a7101c8fa2240769ecdeae4dbcaa3b2eb", null ],
    [ "start_time", "classUI__back__end__lab07__alternate_1_1BackEnd.html#af422f6dd1b6fc51fded3fac2daa563c8", null ],
    [ "state", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a6a77f390c731b6c79b4c368b60956c53", null ],
    [ "step_count", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a75e2933c046d66788db3805a8af41bbc", null ],
    [ "U_input", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a1d1a72849d45ad30f0b50a93c59eb0f3", null ],
    [ "uart", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a68f12b09140b9b013a5962276f71202e", null ],
    [ "value", "classUI__back__end__lab07__alternate_1_1BackEnd.html#afcf36478ef08ddd71d954b70ffbe15bf", null ],
    [ "Velocity_send", "classUI__back__end__lab07__alternate_1_1BackEnd.html#a1bf0e457acce6b936c1d21c52808ac33", null ]
];