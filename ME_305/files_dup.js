var files_dup =
[
    [ "encoder.py", "encoder_8py.html", [
      [ "Encoder", "classencoder_1_1Encoder.html", "classencoder_1_1Encoder" ]
    ] ],
    [ "fibonnaci_lab01.py", "fibonnaci__lab01_8py.html", "fibonnaci__lab01_8py" ],
    [ "Hw0x0.py", "Hw0x0_8py.html", [
      [ "TaskElevator", "classHw0x0_1_1TaskElevator.html", "classHw0x0_1_1TaskElevator" ],
      [ "Button", "classHw0x0_1_1Button.html", "classHw0x0_1_1Button" ],
      [ "MotorDriver", "classHw0x0_1_1MotorDriver.html", "classHw0x0_1_1MotorDriver" ]
    ] ],
    [ "Lab02.py", "Lab02_8py.html", [
      [ "TaskBlinky", "classLab02_1_1TaskBlinky.html", "classLab02_1_1TaskBlinky" ],
      [ "LED_State", "classLab02_1_1LED__State.html", "classLab02_1_1LED__State" ]
    ] ],
    [ "Lab03.py", "Lab03_8py.html", [
      [ "TaskEncoder", "classLab03_1_1TaskEncoder.html", "classLab03_1_1TaskEncoder" ]
    ] ],
    [ "Lab04.py", "Lab04_8py.html", [
      [ "TaskEncoder", "classLab04_1_1TaskEncoder.html", "classLab04_1_1TaskEncoder" ]
    ] ],
    [ "Lab05.py", "Lab05_8py.html", [
      [ "TaskLED", "classLab05_1_1TaskLED.html", "classLab05_1_1TaskLED" ],
      [ "LED_State", "classLab05_1_1LED__State.html", "classLab05_1_1LED__State" ]
    ] ],
    [ "Lab06.py", "Lab06_8py.html", [
      [ "ControllerTask", "classLab06_1_1ControllerTask.html", "classLab06_1_1ControllerTask" ]
    ] ],
    [ "lab06_front_end.py", "lab06__front__end_8py.html", "lab06__front__end_8py" ],
    [ "Lab07.py", "Lab07_8py.html", [
      [ "ControllerTask", "classLab07_1_1ControllerTask.html", "classLab07_1_1ControllerTask" ]
    ] ],
    [ "lab07_front_end.py", "lab07__front__end_8py.html", "lab07__front__end_8py" ],
    [ "main_hw0x0.py", "main__hw0x0_8py.html", "main__hw0x0_8py" ],
    [ "main_lab02.py", "main__lab02_8py.html", "main__lab02_8py" ],
    [ "main_lab03.py", "main__lab03_8py.html", "main__lab03_8py" ],
    [ "main_lab05.py", "main__lab05_8py.html", "main__lab05_8py" ],
    [ "main_lab06.py", "main__lab06_8py.html", "main__lab06_8py" ],
    [ "main_lab07.py", "main__lab07_8py.html", "main__lab07_8py" ],
    [ "Motor_Driver.py", "Motor__Driver_8py.html", "Motor__Driver_8py" ],
    [ "shares.py", "shares_8py.html", "shares_8py" ],
    [ "UI_back_end.py", "UI__back__end_8py.html", [
      [ "BackEnd", "classUI__back__end_1_1BackEnd.html", "classUI__back__end_1_1BackEnd" ]
    ] ],
    [ "UI_back_end_lab07_alternate.py", "UI__back__end__lab07__alternate_8py.html", [
      [ "BackEnd", "classUI__back__end__lab07__alternate_1_1BackEnd.html", "classUI__back__end__lab07__alternate_1_1BackEnd" ]
    ] ],
    [ "User_interface.py", "User__interface_8py.html", [
      [ "TaskUI", "classUser__interface_1_1TaskUI.html", "classUser__interface_1_1TaskUI" ]
    ] ],
    [ "User_interface_lab04.py", "User__interface__lab04_8py.html", [
      [ "TaskUI", "classUser__interface__lab04_1_1TaskUI.html", "classUser__interface__lab04_1_1TaskUI" ]
    ] ]
];