var classLab07_1_1ControllerTask =
[
    [ "__init__", "classLab07_1_1ControllerTask.html#aafc3693dc56ed19fc34b0578c372a65f", null ],
    [ "run", "classLab07_1_1ControllerTask.html#ae28e894919e37d22abd2ef5134beb5e4", null ],
    [ "transitionTo", "classLab07_1_1ControllerTask.html#a199844e09c253a168dccae70b24599cb", null ],
    [ "Controller", "classLab07_1_1ControllerTask.html#a01b543c65dc0382ccc34aa803930c35b", null ],
    [ "curr_time", "classLab07_1_1ControllerTask.html#acc5e44dae5f4d4215759d9a508d8578e", null ],
    [ "EncoderTrack", "classLab07_1_1ControllerTask.html#a9a0d69c273a9974afbf046011b4eed9e", null ],
    [ "interval", "classLab07_1_1ControllerTask.html#a5549415eaeee89c5acf23fdae4e294e7", null ],
    [ "K_p", "classLab07_1_1ControllerTask.html#a6fd2297fdeaad0eb70dfea236913a7c5", null ],
    [ "Motor", "classLab07_1_1ControllerTask.html#a859e102b1915d36533ba07add3125b5d", null ],
    [ "New_duty", "classLab07_1_1ControllerTask.html#a801a3ddf9e45ed48bffe1893d589644e", null ],
    [ "next_time", "classLab07_1_1ControllerTask.html#a263d1dcde12e1971d3f316ee74c15e16", null ],
    [ "Omega_measured", "classLab07_1_1ControllerTask.html#ae463407fe3d121e1e65749b619fa00e4", null ],
    [ "Omega_ref", "classLab07_1_1ControllerTask.html#a87ec145cab280a51c6424e1d9421fc21", null ],
    [ "position", "classLab07_1_1ControllerTask.html#affcd5ce61703fb751db3d9e406841fb9", null ],
    [ "runs", "classLab07_1_1ControllerTask.html#a4533991f80561f2a32a5efb282f2d4ad", null ],
    [ "start_time", "classLab07_1_1ControllerTask.html#af619ff488089b62e49137391cd0e2bac", null ],
    [ "state", "classLab07_1_1ControllerTask.html#aa0673353c73ef2f610f548b55e10bc52", null ]
];