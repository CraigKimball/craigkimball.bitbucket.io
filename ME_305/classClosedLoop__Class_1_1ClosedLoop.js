var classClosedLoop__Class_1_1ClosedLoop =
[
    [ "__init__", "classClosedLoop__Class_1_1ClosedLoop.html#aa4406f036364f02e7f27a417b6bedb98", null ],
    [ "get_Kp", "classClosedLoop__Class_1_1ClosedLoop.html#a7294d054e2665bfa43dd6641b0baa317", null ],
    [ "LoopRun", "classClosedLoop__Class_1_1ClosedLoop.html#a173da3256ee994ef1347fceadfc003cb", null ],
    [ "set_Kp", "classClosedLoop__Class_1_1ClosedLoop.html#ad08b942c8a6698d72b4348797b5878a5", null ],
    [ "Omega_meas", "classClosedLoop__Class_1_1ClosedLoop.html#a6c1345bb8b009bae9397aba752e3f668", null ],
    [ "Omega_ref", "classClosedLoop__Class_1_1ClosedLoop.html#abaebd0c8c48d12e09693bd717d50eb9d", null ],
    [ "Prop_gain", "classClosedLoop__Class_1_1ClosedLoop.html#a487120eb32ad0acec7c8efd2615ef8e5", null ],
    [ "Prop_gain_desired", "classClosedLoop__Class_1_1ClosedLoop.html#a7d4fac564cee487d8830b46cc74f5148", null ]
];