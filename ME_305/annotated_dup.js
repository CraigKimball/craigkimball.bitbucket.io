var annotated_dup =
[
    [ "ClosedLoop_Class", null, [
      [ "ClosedLoop", "classClosedLoop__Class_1_1ClosedLoop.html", "classClosedLoop__Class_1_1ClosedLoop" ]
    ] ],
    [ "encoder", "namespaceencoder.html", "namespaceencoder" ],
    [ "Hw0x0", null, [
      [ "Button", "classHw0x0_1_1Button.html", "classHw0x0_1_1Button" ],
      [ "MotorDriver", "classHw0x0_1_1MotorDriver.html", "classHw0x0_1_1MotorDriver" ],
      [ "TaskElevator", "classHw0x0_1_1TaskElevator.html", "classHw0x0_1_1TaskElevator" ]
    ] ],
    [ "Lab02", null, [
      [ "LED_State", "classLab02_1_1LED__State.html", "classLab02_1_1LED__State" ],
      [ "TaskBlinky", "classLab02_1_1TaskBlinky.html", "classLab02_1_1TaskBlinky" ]
    ] ],
    [ "Lab03", null, [
      [ "TaskEncoder", "classLab03_1_1TaskEncoder.html", "classLab03_1_1TaskEncoder" ]
    ] ],
    [ "Lab04", null, [
      [ "TaskEncoder", "classLab04_1_1TaskEncoder.html", "classLab04_1_1TaskEncoder" ]
    ] ],
    [ "Lab05", null, [
      [ "LED_State", "classLab05_1_1LED__State.html", "classLab05_1_1LED__State" ],
      [ "TaskLED", "classLab05_1_1TaskLED.html", "classLab05_1_1TaskLED" ]
    ] ],
    [ "Lab06", null, [
      [ "ControllerTask", "classLab06_1_1ControllerTask.html", "classLab06_1_1ControllerTask" ]
    ] ],
    [ "Lab07", null, [
      [ "ControllerTask", "classLab07_1_1ControllerTask.html", "classLab07_1_1ControllerTask" ]
    ] ],
    [ "Motor_Driver", null, [
      [ "MotorDriver", "classMotor__Driver_1_1MotorDriver.html", "classMotor__Driver_1_1MotorDriver" ]
    ] ],
    [ "UI_back_end", null, [
      [ "BackEnd", "classUI__back__end_1_1BackEnd.html", "classUI__back__end_1_1BackEnd" ]
    ] ],
    [ "UI_back_end_lab07_alternate", null, [
      [ "BackEnd", "classUI__back__end__lab07__alternate_1_1BackEnd.html", "classUI__back__end__lab07__alternate_1_1BackEnd" ]
    ] ],
    [ "User_interface", null, [
      [ "TaskUI", "classUser__interface_1_1TaskUI.html", "classUser__interface_1_1TaskUI" ]
    ] ],
    [ "User_interface_lab04", null, [
      [ "TaskUI", "classUser__interface__lab04_1_1TaskUI.html", "classUser__interface__lab04_1_1TaskUI" ]
    ] ]
];