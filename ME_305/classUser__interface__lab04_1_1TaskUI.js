var classUser__interface__lab04_1_1TaskUI =
[
    [ "__init__", "classUser__interface__lab04_1_1TaskUI.html#af480d27d4252b0af1e0db91897044fd3", null ],
    [ "run", "classUser__interface__lab04_1_1TaskUI.html#a978f476e9f6152e49dab2c444d3934f5", null ],
    [ "transitionTo", "classUser__interface__lab04_1_1TaskUI.html#acb4f10097f8bfd50a07d44941185fff1", null ],
    [ "curr_time", "classUser__interface__lab04_1_1TaskUI.html#acde42a2370cc13c75d817289b3bdf100", null ],
    [ "EncoderPos", "classUser__interface__lab04_1_1TaskUI.html#a308c1a52c78fc1009b74c53a666066d4", null ],
    [ "interval", "classUser__interface__lab04_1_1TaskUI.html#a8540e9a77debd967ecf90dfdaceb6c23", null ],
    [ "next_time", "classUser__interface__lab04_1_1TaskUI.html#a39c900ad96cbea1d2e8aabd8ad402fc8", null ],
    [ "raw_data", "classUser__interface__lab04_1_1TaskUI.html#a80f06cd806a681620bc69962232b01f6", null ],
    [ "runs", "classUser__interface__lab04_1_1TaskUI.html#a29bf047d6b6cfd29378f2ee213f2deba", null ],
    [ "ser", "classUser__interface__lab04_1_1TaskUI.html#a29947e501ed585e5fa926dc75781c0d6", null ],
    [ "start_time", "classUser__interface__lab04_1_1TaskUI.html#a8a415e7f5ae081dd086ed3c3fb04d4c7", null ],
    [ "state", "classUser__interface__lab04_1_1TaskUI.html#ab83aaae431fae5406234f96ecd56eeb4", null ],
    [ "Time", "classUser__interface__lab04_1_1TaskUI.html#ae9e528f5da5bd93f13883c87a066adfb", null ],
    [ "Timer10", "classUser__interface__lab04_1_1TaskUI.html#a3785713ba837d4461994500f53b29ade", null ]
];